#!/bin/bash

zerotier-one &
# ZTIP="192.168.195.1"
ZT_PID=$!
# MEMBERS=$(curl -X GET -H "Authorization: token $ZTAUTHTOKEN" https://api.zerotier.com/api/v1/network/$NETWORK_ID/member | jq -s '.[][].nodeId')

for NETWORK_ID in $(echo $NETWORK_IDS | sed 's/,/\t/g'); do
  [ ! -z $NETWORK_ID ] && { sleep 5; zerotier-cli join $NETWORK_ID || exit 1; }

  # waiting for Zerotier IP
  # why 2? because you have an ipv6 and an a ipv4 address by default if everything is ok
  IP_OK=0
  while [ $IP_OK -lt 1 ]
  do
    ZTDEV=$( ip l| grep zt | awk '{ print $2 }' | cut -f1 -d':' | tail -1 )
    IP_OK=$( ip addr show dev $ZTDEV | grep -i inet | wc -l )
    sleep 5

    echo $IP_OK

    # # Auto accept the new client
    echo "Auto accept the new client"
    echo
    echo
    HOST_ID="$(zerotier-cli info | awk '{print $3}')"
    # if [[ ${MEMBERS[*]} =~ ${HOST_ID} ]]; then
    #     curl -s -XDELETE \
    #         -H "Authorization: token $ZTAUTHTOKEN" \
    #         "https://my.zerotier.com/api/v1/network/$NETWORK_ID/member/$HOST_ID"
    # fi

    curl -s -XPOST \
        -H "Authorization: token $ZTAUTHTOKEN" \
        -d '{"hidden":"false","config":{"authorized":true}}' \
        "https://my.zerotier.com/api/v1/network/$NETWORK_ID/member/$HOST_ID"
    echo
    echo

    # # If hostname is provided will be set
    if [ ! -z $ZTHOSTNAME ]
    then
        echo "Set hostname"
        curl -s -XPOST \
            -H "Authorization: token $ZTAUTHTOKEN" \
            -d "{\"name\":\"$ZTHOSTNAME\"}" \
            "https://my.zerotier.com/api/v1/network/$NETWORK_ID/member/$HOST_ID"
        echo
        echo
    fi

    echo "Waiting for a ZeroTier IP on $ZTDEV interface... Accept the new host on my.zerotier.com"
  done
  echo "================================================="
done

# Set up Zerotier Router
echo "net.ipv4.ip_forward = 1" >> /etc/sysctl.conf
sysctl -p
PHY_IFACE=eth0; ZT_IFACE=$ZTDEV
iptables -t nat -A POSTROUTING -o $PHY_IFACE -j MASQUERADE
iptables -A FORWARD -i $PHY_IFACE -o $ZT_IFACE -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A FORWARD -i $ZT_IFACE -o $PHY_IFACE -j ACCEPT

# Kill zerotier-one and let supervisor take over
kill $ZT_PID
supervisord -c /etc/supervisor/supervisord.conf
